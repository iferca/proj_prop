#!/usr/bin/env bash

export AWS_PROFILE="sam"

sam local start-api --port 3001 \
                    --region us-east-1 \
                    --docker-network host \
                    --debug \
                    --warm-containers LAZY
