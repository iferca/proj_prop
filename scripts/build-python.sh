#!/usr/bin/env bash

echo
echo "Building Python deployables..."

BASE_PATH="$(pwd)"
LIB_PATH="$BASE_PATH/src/lib/renvance"

mkdir -p "$BASE_PATH/build/python"

functions=("src/functions")

PackageFunctions() {
  f_dir="$1"
  f_name="$(basename "$f_dir")"

  if [ ! -d "$f_dir" ]; then
    echo "Invalid function dir $f_dir. PWD = $PWD"
    exit 1
  fi
  cd "$f_dir" || exit

  pip install -r requirements.txt --target ./package
  pip install "$LIB_PATH" --target ./package
  cp -r ./"$f_dir" ./package
  if [[ -d package ]]; then
    cd package || exit
    find . -name "*.pyc" -delete
    find . -name "*.egg-info" -print0 | xargs rm -rf
    zip -9mrv ../package .
    cd ..
    rm -rf package
    mv package.zip "$BASE_PATH/build/python/$f_name.zip"
  fi
  cd ..
}

cd "$BASE_PATH" || exit

# Iterate over functions
for resource_root in "${functions[@]}"; do
  cd "$resource_root" || exit
  for func_dir in */; do
    PackageFunctions "$func_dir"
  done
done



