from fastapi import FastAPI
from mangum import Mangum

from renvance import business

app = FastAPI()


@app.get("/dev/function2")
def index():
    return {"message": f"Even more {business.do()}"}


handler = Mangum(app, lifespan="off")
