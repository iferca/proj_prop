from renvance import business


def handler(event, context):
    return {
        "statusCode": 200,
        "body": business.do()
    }
