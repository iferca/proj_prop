AWS_PROFILE := "sam"
AWS_REGION := "us-east-2"
ENV := dev

all: $(TARGETS)

.DEFAULT_GOAL := sam_build

test_python:
	pytest

test: test_python

build_python: $(shell find . -name '*.py' -type f)
	AWS_REGION=${AWS_REGION} AWS_PROFILE=${AWS_PROFILE} ./scripts/build-python.sh

build: build_python

sam_build: build
	AWS_PROFILE=$(AWS_PROFILE) sam build

sam_package:
	AWS_PROFILE=${AWS_PROFILE} sam package \
		--region ${AWS_REGION} \
		--output-template-file ./.aws-sam/packaged.yaml

sam_run:
	./scripts/sam-run.sh

build_run: clean sam_build sam_run

sam_deploy: sam_build
	AWS_PROFILE=${AWS_PROFILE} sam deploy --config-file samconfig.toml --config-env ${ENV} --no-confirm-changeset \
		--region ${AWS_REGION} \
		--no-fail-on-empty-changeset \
		--guided

sam_deploy_dev: override ENV = dev
sam_deploy_dev: override AWS_REGION = "us-east-2"
sam_deploy_dev: sam_deploy

clean:
	rm -rf build/python
	rm -rf .aws-sam
